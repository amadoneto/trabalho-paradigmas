const axios = require('axios');

const Functions = {
    getCitiesData: async (latitude, longitude) => {
        const url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyDSKkwcUJtgWbmm_6RCmPw-ZYwiil2ljKE`;

        try {
            const response = await axios.get(url);
            const data = response.data;
            console.log('ewrererere: ', data);

            if (data.status === 'OK') {
                for (const result of data.results) {
                    for (const component of result.address_components) {
                        if (component.types.includes('locality')) {
                            return component.long_name;
                        }
                    }
                }
            }
            return null;
        } catch (error) {
            console.error('Erro ao obter a cidade:', error);
            return null;
        }
    },
    quintuplicateWithData:(originalData, targetCount)=> {
        const multipliedData = [];
        const originalDataLength = originalData.length;
      
        while (multipliedData.length < targetCount) {
          const randomIndex = Math.floor(Math.random() * originalDataLength);
          const newData = JSON.parse(JSON.stringify(originalData[randomIndex]));
      
          newData.id = Math.floor(Math.random() * (9999999 - 1000000) + 1000000);
          newData.dt = Math.floor(Math.random() * (2000000000 - 1500000000) + 1500000000); 
          newData.main.temp = (Math.random() * (40 - (-30)) - 30).toFixed(2); 
          newData.main.humidity = Math.floor(Math.random() * 101); 
      
          multipliedData.push(newData);
        }
      
        return multipliedData.slice(0, targetCount);
      }

}
export default Functions