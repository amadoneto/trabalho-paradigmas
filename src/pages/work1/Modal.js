import React, { useState } from 'react';

const SideModal = (props) => {
    const [isOpen, setIsOpen] = useState(false);
    const { data } = props
    console.log('datas: ', data);

    const openModal = () => {
        setIsOpen(true);
    };

    const closeModal = () => {
        setIsOpen(false);
    };
    const removeModal = idx => {
        let copy = [...data]
        copy.splice(idx, 1)
        props.setData(copy)
    };

    return (
        <div>
            <button onClick={openModal}>Ver Selecionados</button>

            {/* Modal */}
            {isOpen && (
                <div className="side-modal-overlay" onClick={closeModal}>
                    <div className="side-modal-content" onClick={(e) => e.stopPropagation()}>
                        <h2>Cidades Selecionadas</h2>
                        {data && data.map((city, cityIndex) =>

                            <div key={cityIndex} className="city-card">
                                <h4>{city.name}</h4>
                                <p>Temperatura: {city.main?.temp}°C</p>
                                <p>Umidade: {city.main?.humidity}%</p>
                                <p>Velocidade do Vento: {city.wind?.speed} m/s</p>
                                <button onClick={_ => removeModal(cityIndex)}>Remover</button>

                            </div>

                        )}
                        <p>{!data.length && 'Sem cidades selecionadas'}</p>
                        <button onClick={closeModal}>Fechar Modal</button>
                    </div>
                </div>
            )}
        </div>
    );
};

export default SideModal;
