import React, { useEffect, useState } from 'react';
import { useTable, usePagination, useGlobalFilter } from 'react-table';
import moment from 'moment';
import axios, { all } from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Chart } from "react-google-charts";
import { v4 as uuidv4 } from 'uuid';


const Table = ({ columns, data }) => {
    const [currentPage, setCurrentPage] = useState(1);
    const [totalPages, setTotalPages] = useState(1);
    const limitPerPage = 10;
    const API_KEY = '92b9ad54cc7f42bbb14d0f2b65d8cd27';
    const baseApiUrl = 'https://api.weatherbit.io/v2.0/normals';
    const [weatherData, setWeatherData] = useState([]);
    const [startDay, setStartDay] = useState(moment('2023-01-01'))
    const [endDay, setEndDay] = useState(moment('2023-01-10'))
    const [latitude, setLatitude] = useState('-16.3285')
    const [longitude, setLongitude] = useState('-48.9530')
    const [city, setCity] = useState('');
    const [selectedCities, setSelectedCities] = useState([{ id: null }]);
    const [showModal, setShowModal] = useState(false);
    const [allPages, setAllPages] = useState(1)
    const [allHiddenId, setAllHiddenId] = useState(new Array(10).fill().map(() => uuidv4()))

    const nextPageDay = _ => {
        setStartDay(endDay.clone().add(1, 'days'));
        setEndDay(endDay.clone().add(10, 'days'))
    }
    const previousPageDay = _ => {
        setStartDay(startDay.clone().subtract(10, 'days'));
        setEndDay(endDay.clone().subtract(10, 'days'))
    }

    const getCoordinatesFromCityName = async (cityName) => {
        try {
            const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(cityName)}&key=AIzaSyDSKkwcUJtgWbmm_6RCmPw-ZYwiil2ljKE`);
            const data = response.data;

            console.log('data: ', data);
            if (data.status === 'OK') {
                const location = data.results[0].geometry.location;
                setLatitude(location.lat);
                setLongitude(location.lng);
                setStartDay(moment('2023-01-01'))
                setEndDay(moment('2023-01-10'))
                setCurrentPage(1)
            }
            else {

                toast.error('Cidade não encontrada!');

            }
        } catch (error) {
            console.error('Erro ao obter as coordenadas:', error);
        }

        return null;
    };

    const handleCitySearch = async (searchCity) => {
        const coordinates = await getCoordinatesFromCityName(searchCity, API_KEY);

        if (coordinates) {
            setLatitude(coordinates.latitude);
            setLongitude(coordinates.longitude);
            setCurrentPage(1);
        }
    };

    const handleCitySelection = (selectedTemp, city) => {
        let info = selectedTemp.row.original
        let selected = { id: selectedTemp.row.original.hiddenId, infos: { info, city } }
        if (!selectedCities.some(item => item.id === selected.id)) {
            setSelectedCities([...selectedCities, selected]);
        }

    };

    const removeCity = (id) => {
        const updatedCities = selectedCities.filter(city => city.id !== id);
        setSelectedCities(updatedCities);
    };

    useEffect(_ => { console.log('SELECETD', selectedCities) }, [selectedCities])

    const SelectedCitiesModal = () => {
        let selectModal = selectedCities.filter(x => x.id)
        return (
            <div className={`modal ${showModal ? 'show' : ''}`}>
                <div className="modal-content">
                    <span className="close-button" onClick={() => setShowModal(false)}>&times;</span>
                    <h2>Temperaturas Selecionadas</h2>
                    <div className="card-container">
                        {selectModal.length > 0 ? selectModal.map((city, index) => {

                            let minTemp = city.infos.info.min_temp
                            let maxTemp = city.infos.info.max_temp
                            const currentTemp = city.infos.info?.temp;

                            const data = [
                                ['Tipo', 'Temperatura', { role: 'style' }],

                                ['Mínima', minTemp, 'color:#33FFB8'],
                                ['Atual', currentTemp, 'color: #337BFF'],
                                ['Máxima', maxTemp, 'color: #FF5733'],
                            ];

                            const series = data.map((row, index) => ({
                                annotation: {
                                    textStyle: {
                                        fontSize: 12,
                                        color: 'black',
                                    },
                                    datum: {
                                        series: row[0],
                                        value: row[1],
                                    },
                                    style: row[2],
                                    alwaysOutside: true,
                                    seriesIndex: index,
                                    type: 'point',
                                },
                            }));
                            return (
                                <div className="card" key={index}>
                                    <span className="remove-button" onClick={() => removeCity(city.id)}>X</span>
                                    <b>{city.infos.city}</b>
                                    <p>Data: {moment(city.infos.info.day + '-' + city.infos.info.month, 'DD-MM').format('DD/MM')}</p>
                                    <Chart
                                        chartType="ColumnChart"
                                        data={data}
                                        width="300px"
                                        height="200px"
                                        // legendToggle
                                        options={{
                                            title: ' Temperaturas',
                                            legend: 'none',
                                            hAxis: { title: 'Tipo de Temperatura' },
                                            vAxis: { title: 'Temperatura (°C)' },
                                            series,
                                        }}
                                    />
                                </div>
                            )
                        }) : <p>Sem temperaturas selecionadas</p>}
                    </div>
                </div>
            </div>
        );
    };


    useEffect(() => {
        const worker = new Worker('worker_grid.js');

        const fetchData = async () => {
            try {
                const url = `${baseApiUrl}?lat=${latitude}&lon=${longitude}&start_day=${startDay.format('MM-DD')}&end_day=${endDay.format('MM-DD')}&tp=daily&key=${API_KEY}`;

                worker.postMessage({ url, apiKey: API_KEY, currentPage, limit: 2 });
            } catch (error) {
                console.error('Erro ao buscar dados de clima:', error);
            }
        };

        fetchData();

        worker.addEventListener('message', (event) => {
            const { data } = event.data;
            let data_modified = []
            // if (currentPage === 1) {
            //     data_modified = data?.map((item, idx) => ({
            //         ...item,
            //         hiddenId: allHiddenId[idx]
            //     }));
            // }
            if (currentPage > (allHiddenId.length / 10)) {
                let new_hiddens_id = data.map(x => uuidv4())
                new_hiddens_id = allHiddenId.concat(new_hiddens_id)
                setAllHiddenId(new_hiddens_id)
                new_hiddens_id = new_hiddens_id.slice((currentPage * 10) - 10, currentPage * 10)
                data_modified = data?.map((item, idx) => ({
                    ...item,
                    hiddenId: new_hiddens_id[idx]
                }));
            }
            else {
                let concated_hidden = allHiddenId.slice((currentPage * 10) - 10, currentPage * 10)
                data_modified = data?.map((item, idx) => ({
                    ...item,
                    hiddenId: concated_hidden[idx]
                }));
            }

            setWeatherData(data_modified);
            setTotalPages(Math.ceil(data.length / limitPerPage));
        });

        return () => {
            worker.terminate();
        };
    }, [currentPage, latitude, longitude]);

    useEffect(() => {
        console.log('changed lat or long') // Reinicia a paginação para a primeira página ao alterar latitude ou longitude
    }, [latitude, longitude]);

    useEffect(() => {
        // Função assíncrona para obter a cidade a partir da latitude e longitude
        const getCityFromCoordinates = async () => {
            try {
                const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyDSKkwcUJtgWbmm_6RCmPw-ZYwiil2ljKE`);
                const data = response.data;

                if (data.status === 'OK') {
                    for (const result of data.results) {
                        for (const component of result.address_components) {
                            if (component.types.includes('locality')) {
                                setCity(component.long_name); // Define a cidade no estado
                                return;
                            }
                        }
                    }
                }
            } catch (error) {
                console.error('Erro ao obter a cidade:', error);
            }
        };

        getCityFromCoordinates(); // Chama a função para obter a cidade ao carregar a página
    }, [latitude, longitude]);

    const manualGlobalFilter = () => true


    const {
        getTableProps,
        getTableBodyProps,
        prepareRow,
        page,
        headerGroups,
        state: { pageIndex }
    } = useTable(
        {
            columns,
            data: weatherData,
            manualGlobalFilter,
            initialState: {
                defaultPageSize: 10,
                // hiddenColumns: columns.map(item => item.columns.filter(coluna => (
                //     (coluna.apenasRelatorio === true) && (coluna.apenasDataGrid === undefined || coluna.apenasDataGrid === true))).map(coluna => coluna.accessor)).flat(1)
            },
            manualPagination: true,

        },
        // useFilters,
        useGlobalFilter,
        usePagination
    )



    return (
        <>
            <h1>{city}</h1>
            <div className="table-container" style={{ width: '80%', margin: '0 auto' }}>
                <input
                    type="text"
                    placeholder="Pesquisar cidade"
                    onKeyDown={(e) => {
                        if (e.key === 'Enter') {
                            handleCitySearch(e.target.value);
                        }
                    }}
                    className="search-input"
                    style={{
                        width: '100%',
                        padding: '15px',
                        fontSize: '18px',
                        fontWeight: 'bold',
                        borderRadius: '5px',
                        border: '2px solid #337BFF',
                        boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
                        boxSizing: 'border-box',
                        marginBottom: '20px',
                    }}
                />
                <div style={{ overflowX: 'auto' }}>
                    <table {...getTableProps()} className="grid-table" style={{ width: '100%' }}>
                        {/* Cabeçalho da tabela */}
                        <thead>
                            {headerGroups.map((headerGroup) => (
                                <tr {...headerGroup.getHeaderGroupProps()}>
                                    {headerGroup.headers.map((column) => {
                                        if (column.id !== 'id') {
                                            return (
                                                <th {...column.getHeaderProps()}>{column.render('Header')}</th>
                                            );
                                        }
                                        return null;
                                    })}
                                </tr>
                            ))}
                        </thead>
                        {/* Corpo da tabela */}
                        <tbody {...getTableBodyProps()}>
                            {page.map((row, i) => {
                                prepareRow(row);
                                return (
                                    <tr {...row.getRowProps()}
                                        style={{ backgroundColor: selectedCities.some(item => item.id === row.original.hiddenId) ? 'lightblue' : 'inherit' }}
                                    >
                                        {row.cells.map((cell) => (
                                            <td {...cell.getCellProps()}
                                                style={{ padding: '0.5rem', borderBottom: '1px solid #ddd', cursor: 'pointer' }}
                                                onClick={() => handleCitySelection(cell, city)}
                                                key={cell.getCellProps().key}
                                            >{cell.render('Cell')}</td>
                                        ))}
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
                <div className="pagination">
                    <button style={{
                        padding: '10px 20px',
                        fontSize: '16px',
                        fontWeight: 'bold',
                        backgroundColor: '#337BFF',
                        color: 'white',
                        border: 'none',
                        borderRadius: '5px',
                        cursor: 'pointer',
                        boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
                    }}
                        onClick={() => {
                            let page = currentPage - 1;
                            previousPageDay();
                            setCurrentPage(page);
                        }} disabled={currentPage === 1}>
                        Anterior
                    </button>
                    <span>
                        Página{' '}
                        <strong>
                            {currentPage}
                        </strong>{' '}
                    </span>
                    <button
                        style={{
                            padding: '10px 20px',
                            fontSize: '16px',
                            fontWeight: 'bold',
                            backgroundColor: '#337BFF',
                            color: 'white',
                            border: 'none',
                            borderRadius: '5px',
                            cursor: 'pointer',
                            boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
                        }}
                        onClick={() => {
                            let page = currentPage + 1;
                            nextPageDay();
                            setCurrentPage(page);
                        }}>
                        Próxima
                    </button>
                </div>
                <div style={{ marginTop: '20px', textAlign: 'center' }}>
                    <button
                        onClick={() => setShowModal(true)}
                        style={{
                            padding: '10px 20px',
                            fontSize: '16px',
                            fontWeight: 'bold',
                            backgroundColor: '#337BFF',
                            color: 'white',
                            border: 'none',
                            borderRadius: '5px',
                            cursor: 'pointer',
                            boxShadow: '0 4px 6px rgba(0, 0, 0, 0.1)',
                        }}
                    >
                        {`Mostrar Temperaturas Selecionadas (${selectedCities.filter(x => x.id).length})`}
                    </button>
                </div>
            </div>
            <SelectedCitiesModal />
        </>
    );

};

export default Table;
