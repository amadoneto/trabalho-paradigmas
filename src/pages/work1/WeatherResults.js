import React from 'react';
import Table from './Table';
import '../../Grid.css'
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';

const API_KEY = '92b9ad54cc7f42bbb14d0f2b65d8cd27';
const url = `https://api.weatherbit.io/v2.0/normals?lat=-16.3285&lon=-48.9530&start_day=01-01&end_day=01-10&tp=daily&key=${API_KEY}`;
const uniqueId = uuidv4();


const columns = [
  {
    Header: 'Dia',
    accessor: 'day',
    Cell: ({ row }) => {
      const month = row.original.month
      let day_formatted = moment(row.original.day + '- ' + month, 'DD - MM').format('DD/MM')
      return `${day_formatted}`;
    },
  },
  {
    Header: 'Max Temp',
    accessor: 'max_temp',
    Cell : ({row}) => {
        return row.original.max_temp + '°'
    }
  },
  {
    Header: 'Min Temp',
    accessor: 'min_temp',
    Cell : ({row}) => {
      return row.original.min_temp + '°'
  }
  },
  {
    Header: 'Temperatura Atual',
    accessor: 'temp',
    Cell : ({row}) => {
      return row.original.temp + '°'
  }
  },
  {
    Header: 'Precipitação',
    accessor: 'precip',
  },
  // Adicione mais colunas conforme necessário para corresponder aos seus dados
];


const WeatherResults = () => {
  return (
    <div className="table-container">
      <Table columns={columns} />
    </div>
  );
};

export default WeatherResults