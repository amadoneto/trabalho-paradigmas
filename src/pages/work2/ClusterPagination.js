import React, { useState } from 'react';
import ReactPaginate from 'react-paginate';
import { Container, Typography, Grid, Card, Button, Modal, Box } from '@mui/material';
import { Chart } from "react-google-charts";
import SideModal from '../work1/Modal'


const ClustersPagination = ({ processedData }) => {
    // const ITEMS_PER_PAGE = 5;

    const [currentPage, setCurrentPage] = useState(0);
    const pageCount = processedData.length;

    const [selectedCity, setSelectedCity] = useState(null);
    const [selectedCities, setSelectedCities] = useState([]);


    const handlePageClick = ({ selected }) => {
        setCurrentPage(selected);
    };

    const selectCities = (obj) => {
        let copy = [...selectedCities]
        copy.push(obj)
        setSelectedCities(copy)
    }

    const handleCloseModal = _ => setSelectedCity(null)


    const displayData = processedData?.map((cluster, index) => (
        <div key={index} className="cluster">
            <h3>Cluster {index + 1}</h3>
            <div className="city-grid">
                {cluster.map((city, cityIndex) => (
                    <div key={cityIndex} className="city-card">
                        <h4>{city.name}</h4>
                        <p>Temperatura: {city.main?.temp}°C</p>
                        <p>Umidade: {city.main?.humidity}%</p>
                        <p>Velocidade do Vento: {city.wind?.speed} m/s</p>
                        <button onClick={() => setSelectedCity(city)}>Detalhes</button>
                        <button onClick={() => selectCities(city)}>Selecionar</button>
                    </div>
                ))}
            </div>
        </div>
    ));

    const renderModal = () => {

        const minTemp = selectedCity?.main?.temp_min - 2;
        const maxTemp = selectedCity?.main?.temp_max + 2;
        const currentTemp = selectedCity?.main?.temp;

        const data = [
            ['Tipo', 'Temperatura', { role: 'style' }],

            ['Mínima', minTemp, 'color:#33FFB8'],
            ['Atual', currentTemp, 'color: #337BFF'],
            ['Máxima', maxTemp, 'color: #FF5733'],
        ];

        const series = data.map((row, index) => ({
            annotation: {
                textStyle: {
                    fontSize: 12,
                    color: 'black',
                },
                datum: {
                    series: row[0],
                    value: row[1],
                },
                style: row[2],
                alwaysOutside: true,
                seriesIndex: index,
                type: 'point',
            },
        }));

        return (
            <Modal open={!!selectedCity} onClose={handleCloseModal}>
                <Box sx={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', width: 400, bgcolor: 'background.paper', boxShadow: 24, p: 4 }}>
                    <Typography variant="h4" component="div" gutterBottom>
                        {selectedCity?.name}
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Temperatura: <strong>{selectedCity?.main?.temp}°C</strong>
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Sensação Térmica: <strong>{selectedCity?.main?.feels_like}°C</strong>
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Umidade: <strong>{selectedCity?.main?.humidity}%</strong>
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Descrição: <strong>{selectedCity?.weather[0]?.description.toUpperCase()}</strong>
                    </Typography>
                    <Typography variant="body1" gutterBottom>
                        Velocidade Vento: <strong> {selectedCity?.wind.speed} m/s</strong>
                    </Typography>
                    <Chart
                        chartType="ColumnChart"
                        data={data}
                        width="100%"
                        height="200px"
                        // legendToggle
                        options={{
                            title: ' Temperaturas',
                            legend: 'none',
                            hAxis: { title: 'Tipo de Temperatura' },
                            vAxis: { title: 'Temperatura (°C)' },
                            series,
                        }}
                    />
                    <Button variant="contained" onClick={handleCloseModal}>Fechar</Button>
                </Box>
            </Modal>
        );
    };

    return (
        <div>
            {displayData}
            {<SideModal data={selectedCities} setData= {setSelectedCities} />}
            {renderModal()}
           
        </div>
    );
};

export default ClustersPagination;
