import axios from 'axios';

const API_KEY = 'e3df996de8f74ef1a016a39fb2da139f';
const BASE_URL = 'https://api.openweathermap.org/data/2.5';
const WeatherService = {
    getCitiesData: async () => {
        try {
            const maxCities = 2000; // Número desejado de cidades
            const sections = 10; // Número de seções para dividir a área
            let citiesData = [];
            let totalCities = 0;

            for (let i = 0; i < sections && totalCities < maxCities; i++) {
                for (let j = 0; j < sections && totalCities < maxCities; j++) {
                    const lonLeft = -74 + (i * 4);
                    const latBottom = -34 + (j * 4);
                    const lonRight = lonLeft + 4;
                    const latTop = latBottom + 4;

                    const response = await axios.get(`${BASE_URL}/box/city?bbox=${lonLeft},${latBottom},${lonRight},${latTop},10&lang=pt_br&appid=${API_KEY}`);
                    console.log('response: ', response);

                    if (!response || !response.data || !response.data.list || response.data.list.length === 0) {
                        break;
                    }

                    const newData = response.data.list;
                    citiesData = citiesData.concat(newData);
                    totalCities += newData.length;
                }
            }

            return citiesData.slice(0, maxCities); // Retorna apenas o número desejado de cidades
        } catch (error) {
            throw new Error(error.response ? error.response.data.message : error.message);
        }
    },
};

export default WeatherService;
