import React, { useEffect, useState, useRef } from 'react';
import Functions from '../util/functions';
import WeatherService from './WeatherService'; // Verifique o caminho correto para o arquivo
import jsonData from '../util/city.list.json'

const k = 3 // DEFINA AQUI A QUANTIDADE DE WORKERS

const Weather = () => {
    const [citiesData, setCitiesData] = useState([]);
    const [processedData, setProcessedData] = useState([])
    const sharedBuffer = useRef(null);
    const [hasFetchedData, setHasFetchedData] = useState(false);
    const API_KEYS = ['2eeaf1a56b4661503823c9b92c92524f', 'e3df996de8f74ef1a016a39fb2da139f', 'a37f4fc3c98f6271d15b360fb5911caa', '3365fb8bd653f28d190bea843c47fbed']

    const fetchData = async (cities_names) => {
        try {
            setHasFetchedData(true)
            const maxCities = 100;
            const numWorkers = k;
            const sections = numWorkers * 6;
            const citiesPerSection = Math.ceil(maxCities / sections);
            const sharedBuffer = new ArrayBuffer(Float32Array.BYTES_PER_ELEMENT * maxCities * 6);
            const chunkSize = Math.ceil(cities_names.length / numWorkers);
            const cityChunks = chunkArray(cities_names, chunkSize);


            const workerPromises = [];
            const workerCities = [];

            for (let i = 0; i < numWorkers; i++) {
                const worker = new Worker('worker_get.js');
                const startCityIndex = i * citiesPerSection;
                const endCityIndex = Math.min((i + 1) * citiesPerSection, maxCities); // Índice final

                const randomAPIKeyIndex = Math.floor(Math.random() * API_KEYS.length);
                const randomAPIKey = API_KEYS[randomAPIKeyIndex];

                const workerSharedBuffer = new ArrayBuffer(sharedBuffer.byteLength);
                const workerFloat32Array = new Float32Array(workerSharedBuffer);
                workerFloat32Array.set(new Float32Array(sharedBuffer));

                worker.onmessage = null;

                worker.postMessage({
                    workerIndex: i,
                    numWorkers,
                    sharedBuffer: workerSharedBuffer,
                    randomAPIKey,
                    startCityIndex,
                    endCityIndex,
                    citiesArray: cityChunks[i],
                });

                const promise = new Promise((resolve) => {
                    worker.onmessage = function (e) {
                        const { workerIndex, cities, sharedBuffer: workerSharedBuffer } = e.data;
                        if (workerIndex !== undefined && cities !== undefined) {
                            workerCities.push(...cities);

                            // Use o SharedArrayBuffer retornado pelo worker
                            const float32Array = new Float32Array(workerSharedBuffer);
                            const parsedData = [];

                            for (let i = 0; i < float32Array.length; i += 6) {
                                parsedData.push({
                                    temperature: float32Array[i],
                                    humidity: float32Array[i + 1],
                                    windSpeed: float32Array[i + 2],
                                });
                            }


                            setCitiesData(workerCities)

                            resolve();
                        } else {
                            console.error('Dados inválidos recebidos do worker.');
                            resolve();
                        }
                    };
                });

                workerPromises.push(promise);
            }

            await Promise.all(workerPromises);

            let citiesData = [];
            workerCities.forEach((city) => {
                citiesData.push(city);
            });

            const float32Array = new Float32Array(sharedBuffer);
            const parsedData = [];

            for (let i = 0; i < float32Array.length; i += 6) {
                parsedData.push({
                    temperature: float32Array[i],
                    humidity: float32Array[i + 1],
                    windSpeed: float32Array[i + 2],
                });
            }

            const jsonData = JSON.stringify(parsedData);
            localStorage.setItem('weatherData', jsonData);
        } catch (error) {
            console.error('Erro ao buscar dados da API:', error);
        }
    };

    useEffect(() => {
        if (!hasFetchedData) {
            let names_citys = JSON.stringify(jsonData)
            let parsed_names = JSON.parse(names_citys)
            parsed_names = parsed_names.filter(x => x.country === 'BR').map(x => x.name)
            fetchData(parsed_names);
        }
    }, [hasFetchedData]);

    function chunkArray(array, size) {
        const chunkedArray = [];
        for (let i = 0; i < array.length; i += size) {
            const chunk = array.slice(i, i + size);
            chunkedArray.push(chunk);
        }
        return chunkedArray;
    }

    useEffect(() => {
        try {
            if (!citiesData || citiesData.length === 0) {
                console.error('Dados das cidades vazios ou não definidos');
                return;
            }

            const workers = [];
            const segments = chunkArray(citiesData, Math.ceil(citiesData.length / k));
            const jsonData = JSON.stringify(segments);

            const buffer = new ArrayBuffer(jsonData.length * 2);

            const sharedArrayBuffer = new Int8Array(buffer);

            // Converte a string JSON em uma sequência de bytes e a armazena no SharedArrayBuffer
            for (let i = 0; i < jsonData.length; i++) {
                sharedArrayBuffer[i] = jsonData.charCodeAt(i);
            }

            const clusterBuffer = new ArrayBuffer(Float32Array.BYTES_PER_ELEMENT * k * citiesData.length * 6);
            const float32ClusterView = new Float32Array(clusterBuffer);
            // const completedWorkers = new Int32Array(new ArrayBuffer(Int32Array.BYTES_PER_ELEMENT));

            //Devido a politica de restrições no uso do SharedArrayBuffer a funcionalidade foi desativada
            //Na minha máquina descomentando essa parte roda e contempla o que foi pedido, porém ao buildar para o docker sempre retorna SharedArrayBuffer' is not defined; Atomic' is not defined
            // const clusterBuffer = new SharedArrayBuffer(Float32Array.BYTES_PER_ELEMENT * k * citiesData.length * 6);
            // const float32ClusterView = new Float32Array(clusterBuffer);
            const synchronizeClusters = async () => {
                const barrier = new Promise((resolve) => {
                    workers.forEach((worker, workerIndex) => {
                        worker.onmessage = function (e) {
                            const { clusters: workerClusters, workerIndex: receivedIndex, cityInformation } = e.data;
                            console.log(`Clusters do Worker ${receivedIndex}:`, workerClusters, "Infos", cityInformation);

                            const clusterOffset = receivedIndex * citiesData.length * 6;
                            float32ClusterView.set(workerClusters, clusterOffset);
                        };
                    });
                    // Atomics.add(completedWorkers, 0, 1); 
                    // if (Atomics.load(completedWorkers, 0) === k) {
                    //     resolve();
                    // }
                });

                workers.forEach((worker, i) => {
                    worker.postMessage({
                        citiesData: segments[i],
                        k,
                        workerIndex: i,
                        sharedBuffer: sharedArrayBuffer
                    });
                });

                await barrier; // Synchronize workers
            };

            for (let i = 0; i < k; i++) {
                const worker = new Worker('worker.js');
                workers.push(worker);

                worker.onerror = function (error) {
                    console.error('Erro no Worker:', error);
                };
            }

            synchronizeClusters();

            return () => {
                workers.forEach((worker) => worker.terminate());
            };
        } catch (e) {
            console.log('ERRO ', e);
        }
    }, [citiesData]);

    return (
        <div className="container">
            <h4>Checar Console</h4>
            <div className="clusters">
                {/* {processedData && <ClustersPagination processedData={processedData} />} */}
            </div>

        </div>
    );


}

export default Weather;