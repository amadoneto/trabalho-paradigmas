import React, { useState } from 'react';
import './App.css';
import WeatherResults from './pages/work1/WeatherResults';
import Weather from './pages/work2/MainWork'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  const [selectedOption, setSelectedOption] = useState(null);

  const handleOptionSelect = (option) => {
    setSelectedOption(option);
  };

  const renderSelectedOption = () => {
    switch (selectedOption) {
      case 1:
        return (
          <div>
            <WeatherResults />
            <ToastContainer />
          </div>
        );
      case 2:
        return <Weather/>;
      default:
        return null;
    }
  };

  return (
    <div className="App">
      <div className="title-container">
        <h1>Trabalho de Paradigmas</h1>
        <b>Amado Pinheiro</b> <br/>
        <b>Paulo David</b>
      </div>
      <div className="container">
        <div className="box box-part-1" onClick={() => handleOptionSelect(1)}>
          <h2>Parte 1</h2>
        </div>
        <div className="divider"></div> {/* Linha divisória */}
        <div className="box box-part-2" onClick={() => handleOptionSelect(2)}>
          <h2>Parte 2</h2>
        </div>
      </div>
      <div className="rendered-content">
        {renderSelectedOption()}
      </div>
    </div>
  );
}

export default App;
