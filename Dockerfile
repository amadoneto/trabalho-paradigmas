# Use uma imagem base com Node.js
FROM node:14

# Defina o diretório de trabalho dentro do contêiner
WORKDIR /usr/src/app

# Copie os arquivos de configuração
COPY package*.json ./


# Instale as dependências
RUN npm install

# Copie o restante dos arquivos
COPY . .

# Construa o aplicativo React
RUN npm run build

# Expõe a porta 3001
EXPOSE 3001

# Comando para iniciar o aplicativo quando o contêiner for executado
CMD ["npm", "start"]
