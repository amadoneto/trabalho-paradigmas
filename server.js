const express = require('express');
const app = express();
const port = 3001; // Escolha a porta que deseja usar

// Defina rotas ou lógica do servidor aqui

app.listen(port, () => {
  console.log(`Servidor está rodando na porta ${port}`);
});
