const processCities = async (startCityIndex, endCityIndex, citiesArray, sharedBuffer, apiKey) => {
    const citiesData = [];
    const float32Array = new Float32Array(sharedBuffer);

    for (let i = 0; i < 2000; i++) {
        const cityName = citiesArray[i];
        const cityData = await fetchCityData(cityName, apiKey);
        if (cityData?.main) {
            const sharedIndex = (i - startCityIndex) * 6; 
            float32Array[sharedIndex] = cityData.main.temp; 
            float32Array[sharedIndex + 1] = cityData.main.humidity; 
            float32Array[sharedIndex + 2] = cityData.wind.speed; 

            citiesData.push(cityData)
        }
    }

    return { citiesData, float32Array };
};

const fetchCityData = async (cityName, apiKey) => {
    try {
        const apiUrl = `https://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=${apiKey}`;
        const response = await fetch(apiUrl);
        const data = await response.json();
        return data; // Retorna os dados da cidade
    } catch (error) {
        console.error('Erro ao buscar dados da cidade:', error);
        return null;
    }
};


self.onmessage = async (e) => {
    const { workerIndex, numWorkers, sharedBuffer, randomAPIKey, citiesArray } = e.data;
    const maxCities = 2000;

    const citiesPerSection = Math.ceil(maxCities / numWorkers);

    const startCityIndex = workerIndex * citiesPerSection;
    const endCityIndex = Math.min(startCityIndex + citiesPerSection, maxCities);

    const processedData = await processCities(startCityIndex, endCityIndex, citiesArray, sharedBuffer, randomAPIKey);

    self.postMessage({ workerIndex, cities: processedData.citiesData, sharedBuffer: processedData.float32Array });
};