self.addEventListener('message', function (e) {
    const { citiesData, k, workerIndex, sharedBuffer } = e.data;

    if (!citiesData || citiesData.length === 0) {
        console.error('Dados das cidades vazios ou não definidos');
        return;
    }

    let retrievedData = '';

    for (let i = 0; i < sharedBuffer.length && sharedBuffer[i] !== 0; i++) {
        retrievedData += String.fromCharCode(sharedBuffer[i]);
    }
    
    const startIndex = workerIndex * Math.ceil(citiesData.length / k);
    const endIndex = Math.min((workerIndex + 1) * Math.ceil(citiesData.length / k), citiesData.length);
    const citiesSubset = citiesData.slice(startIndex, endIndex);

    const float32View = new Float32Array(sharedBuffer);

    citiesSubset.forEach((city, index) => {
        const offset = (startIndex + index) * 6;
        float32View[offset] = city.main.temp;
        float32View[offset + 1] = city.main.humidity;
        float32View[offset + 2] = city.wind.speed;
    });

    function euclideanDistance(pointA, pointB) {
        return Math.sqrt(
            Math.pow(pointB.main.temp - pointA.main.temp, 2) +
            Math.pow(pointB.main.humidity - pointA.main.humidity, 2) +
            Math.pow(pointB.wind.speed - pointA.wind.speed, 2)
        );
    }

    function initCentroids(data, k) {
        const centroids = [];
        const randomIndices = [];
        while (centroids.length < k) {
            const randomIndex = Math.floor(Math.random() * data.length);
            if (!randomIndices.includes(randomIndex)) {
                randomIndices.push(randomIndex);
                centroids.push(data[randomIndex]);
            }
        }
        return centroids;
    }

    let centroids = initCentroids(citiesSubset, k);
    let clusters = [];

    function assignToCluster(data, centroids) {
        const clusters = new Array(k).fill().map(() => []);
        data.forEach((point) => {
            let minDistance = Infinity;
            let clusterIndex = -1;
            centroids.forEach((centroid, index) => {
                const distance = euclideanDistance(centroid, point);
                if (distance < minDistance) {
                    minDistance = distance;
                    clusterIndex = index;
                }
            });
            clusters[clusterIndex].push(point);
        });
        return clusters;
    }

    function recalculateCentroids(clusters) {
        return clusters.map((cluster) => {
            const clusterSize = cluster.length;
            const clusterCenter = cluster.reduce((acc, point) => {
                return {
                    main: {
                        temp: acc.main.temp + point.main.temp,
                        humidity: acc.main.humidity + point.main.humidity,
                    },
                    wind: {
                        speed: acc.wind.speed + point.wind.speed,
                    },
                };
            });

            return {
                main: {
                    temp: clusterCenter.main.temp / clusterSize,
                    humidity: clusterCenter.main.humidity / clusterSize,
                },
                wind: {
                    speed: clusterCenter.wind.speed / clusterSize,
                },
            };
        });
    }

    function shouldStop(newCentroids, centroids, threshold = 0.0001) {
        for (let i = 0; i < newCentroids.length; i++) {
            const oldCentroid = centroids[i];
            const newCentroid = newCentroids[i];

            const diff = Math.sqrt(
                Math.pow(newCentroid.main.temp - oldCentroid.main.temp, 2) +
                Math.pow(newCentroid.main.humidity - oldCentroid.main.humidity, 2) +
                Math.pow(newCentroid.wind.speed - oldCentroid.wind.speed, 2)
            );

            if (diff > threshold) {
                return false;
            }
        }
        return true;
    }

    for (let i = 0; i < 10; i++) {
        clusters = assignToCluster(citiesSubset, centroids);
        const newCentroids = recalculateCentroids(clusters);
        if (shouldStop(newCentroids, centroids)) {
            break;
        }
        centroids = newCentroids;
    }


    const cityInformation = clusters.map(cluster => {
        return cluster.map(city => {
            return {
                temperature: city.main.temp,
                humidity: city.main.humidity,
                windSpeed: city.wind.speed
            };
        });
    })
    const clusterFloat32Array = new Float32Array(clusters.flat().map(city => [city.main.temp, city.main.humidity, city.wind.speed]).flat());
    self.postMessage({ clusters: clusterFloat32Array, workerIndex, cityInformation });

});
