self.addEventListener('message', async event => {
    const { url, apiKey, currentPage, limit } = event.data;
  
    console.log('currentPage: ', currentPage);
    fetch(`${url}`, {
      method: 'GET',
    //   headers: {
    //     'X-RapidAPI-Key': apiKey,
    //     'X-RapidAPI-Host': 'climate-news-feed.p.rapidapi.com',
    //   }
    })
    .then(response => response.json())
    .then(data => {
        console.log('data: ', data.data);
      self.postMessage(data);
    })
    .catch(error => {
      console.error('Erro ao buscar os dados:', error);
    });
  });